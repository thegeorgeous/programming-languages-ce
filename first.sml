(* This is a comment *)

val x = 34;
(* static environment: x : int *)
(* dynamic environment: x --> 34 *)

val y = 17;
(* static environment: y : int *)
(* dynamic environment: y --> 17 *)

val z = (x+y) + (y+2);
(* static environment: z : int *)
(* dynamic environment: z --> 70 *)

val q = z+1;
(* static environement: q : int *)
(* dynamic environment: q --> 71 *)

val abs_of_z  = if z < 0 then 0 - z else z;
(* static environement: abs_of_z : int *)
(* dynamic environement: abs_of_z --> 70 *)

val test = 12 + (if 28 > 56 then 18 else 11)
