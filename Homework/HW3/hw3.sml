(* Coursera Programming Languages, Homework 3, Provided Code *)

exception NoAnswer
datatype pattern = Wildcard
		             | Variable of string
		             | UnitP
		             | ConstP of int
		             | TupleP of pattern list
		             | ConstructorP of string * pattern

datatype valu = Const of int
	            | Unit
	            | Tuple of valu list
	            | Constructor of string * valu

fun g f1 f2 p =
    let
	      val r = g f1 f2
    in
	      case p of
	          Wildcard          => f1 ()
	        | Variable x        => f2 x
	        | TupleP ps         => List.foldl (fn (p,i) => (r p) + i) 0 ps
	        | ConstructorP(_,p) => r p
	        | _                 => 0
    end

(**** for the challenge problem only ****)

datatype typ = Anything
	           | UnitT
	           | IntT
	           | TupleT of typ list
	           | Datatype of string

                               (**** you can put all your code here ****)
fun only_capitals (words: string list) =
    List.filter (fn word=> Char.isUpper(String.sub(word, 0))) words

fun longest_string1 (words: string list) =
    foldl (fn(x,y) => if String.size(x) > String.size(y) then x else y) "" words

fun longest_string2 (words: string list) =
    foldl (fn(x,y) => if String.size(x) >= String.size(y) then x else y) "" words

fun longest_string_helper f words =
    foldl (fn(x, y) => if f(String.size x, String.size y) then x else y)  "" words

val longest_string3 = longest_string_helper (fn(x,y) => x > y)
val longest_string4 = longest_string_helper (fn(x,y) => x >= y)

val longest_capitalized = longest_string3 o only_capitals

val rev_string = String.implode o List.rev o String.explode

fun first_answer _ [] =
    raise NoAnswer
  | first_answer f (x::xs) =
    case f(x) of
        NONE => first_answer f xs
      | SOME v => v

fun all_answers _ [] =
    SOME []
  | all_answers f lst =
    let
        fun helper([], acc) = SOME acc
          | helper(x::xs, acc) =
            case x
             of NONE => NONE
              | SOME v => helper(xs, v @ acc)
    in
        helper(map f lst, [])
    end

val count_wildcards = g (fn x => 1) (fn x => 0)

val count_wild_and_variable_lengths = g (fn x => 1) (fn x => String.size x)

fun count_some_var(word, p) =
    g (fn x => 0) (fn x => if x = word then 1 else 0) p


val check_pat =
    let
        fun get_variables p =
            case p
             of Variable x => [x]
	            | TupleP ps => List.concat (map get_variables ps)
	            | ConstructorP(_,p) => get_variables p
	            | _ => []

        fun duplicates [] =
            false
          | duplicates (x::xs) = List.exists (fn y => x = y) xs orelse duplicates xs
    in
        not o duplicates o get_variables
    end

fun match value_pattern =
    case value_pattern
     of (_, Wildcard) => SOME []
      | (v, Variable s) => SOME [(s, v)]
      | (Unit, UnitP) => SOME []
      | (Const v, ConstP v') => if v = v' then SOME [] else NONE
      | (Tuple vs, TupleP ps) =>
        if length(vs) = length(ps)
        then all_answers match (ListPair.zip(vs, ps))
        else NONE
      | (Constructor(s2, v), ConstructorP(s1, p)) =>
        if s1 = s2
        then match(v, p)
        else NONE
      | _ => NONE

fun first_match v ptrnlist =
    SOME(first_answer (fn p => match(v, p)) ptrnlist)
    handle NoAnswer => NONE
