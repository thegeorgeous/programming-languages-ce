(* Dan Grossman, Coursera PL, HW2 Provided Code *)

(* if you use this function to compare two strings (returns true if the same
   string), then you avoid several of the functions in problem 1 having
   polymorphic types that may be confusing *)
fun same_string (s1 : string, s2 : string) =
    s1 = s2

(* put your solutions for problem 1 here *)

fun all_except_option (x: string, xs: string list) =
    let fun substr (ys: string list) =
            case ys of
                [] => []
              | ys_hd::ys_tl => if same_string(ys_hd, x)
                                then substr(ys_tl)
                                else ys_hd::substr(ys_tl)
        val result = substr(xs)
    in
        if result = xs then NONE else SOME result
    end

fun get_substitutions1 (substitutions: string list list, s: string) =
    case substitutions of
        [] => []
      | substitution::substitution_tl => case all_except_option(s, substitution) of
                                             NONE => get_substitutions1(substitution_tl, s)
                                           | SOME sub_list =>  sub_list @ get_substitutions1(substitution_tl, s)

fun get_substitutions2 (substitutions: string list list, s: string) =
    let fun get_substitution (substitution_list, acc) =
            case substitution_list of
                [] => acc
              | substitution::substitution_tl => case all_except_option(s, substitution) of
                                                     NONE => get_substitution(substitution_tl, acc)
                                                   | SOME sub_list => get_substitution(substitution_tl, acc @ sub_list)
    in get_substitution(substitutions, [])
    end

fun similar_names (substitutions: string list list, {first: string, middle: string, last: string}) =
    let
        fun get_alternates (sub_list) =
            case sub_list of
                [] => []
              | sub_hd::sub_tl => {first=sub_hd, middle=middle, last=last}::get_alternates(sub_tl)

        val substitutions = get_substitutions2(substitutions, first)
    in
        {first=first, middle=middle, last=last}::get_alternates(substitutions)
    end
(* you may assume that Num is always used with values 2, 3, ..., 10 *)
(*    though it will not really come up *)
datatype suit = Clubs | Diamonds | Hearts | Spades
datatype rank = Jack | Queen | King | Ace | Num of int
type card = suit * rank

datatype color = Red | Black
datatype move = Discard of card | Draw

exception IllegalMove

(* put your solutions for problem 2 here *)
fun card_color ((s, r) : card) =
    case s of
        Clubs => Black
      | Diamonds => Red
      | Hearts => Red
      | Spades => Black

fun card_value ((s, r): card) =
    case r of
        Num value => value
      | Ace => 11
      | _ => 10

fun remove_card (cs: card list, c: card, e: exn) =
    case cs of
        [] => raise e
      | cs_hd::cs_tl => if cs_hd = c
                        then cs_tl
                        else cs_hd::remove_card(cs_tl, c, e)

fun all_same_color (cs: card list) =
    case cs of
        [] => true
      | _::[] => true
      | head::(neck::rest) => card_color(head) = card_color(neck) andalso
                            all_same_color(neck::rest)

fun sum_cards (cs: card list) =
    let fun card_counter (card_list, acc: int) =
            case card_list of
                [] => acc
              | head::card_list_rest => card_counter(card_list_rest, acc + card_value(head))
    in card_counter(cs, 0)
    end

fun score (held_cards: card list, goal: int) =
    let
        val sum = sum_cards(held_cards)
        val prelim_score = if sum > goal
                           then 3 * (sum - goal)
                           else (goal - sum)
    in
        if all_same_color(held_cards)
        then prelim_score div 2
        else prelim_score
    end

fun officiate (card_list: card list, move_list: move list, goal: int) =
    let
        fun play (current_card_list: card list,
                  held_list: card list,
                  current_move_list: move list) =
            let
                fun draw (draw_card_list, move_list_tl) =
                    case current_card_list of
                        [] => score(held_list, goal)
                      | drawn_card::card_list_tl => if sum_cards(drawn_card::held_list) > goal
                                                    then score(drawn_card::held_list, goal)
                                                    else play(card_list_tl, drawn_card::held_list, move_list_tl)
            in
                case current_move_list of
                    [] => score(held_list, goal)
                  | (Discard c)::move_list_tl => play(current_card_list, remove_card(held_list,c,IllegalMove), move_list_tl)
                  | Draw::move_list_tl => draw(current_card_list, move_list_tl)
            end
    in play(card_list, [], move_list)
    end
